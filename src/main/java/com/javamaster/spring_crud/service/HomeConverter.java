package com.javamaster.spring_crud.service;

import com.javamaster.spring_crud.dto.HomeDto;
import com.javamaster.spring_crud.entity.Home;
import org.springframework.stereotype.Component;


@Component
public class HomeConverter {
    public Home fromHomeDtoToHome(HomeDto homeDto) {
        Home home = new Home();
        home.setHome_id(homeDto.getHome_id());
        home.setHome_name(homeDto.getHome_name());
        return home;
    }

    public HomeDto fromHomeToHomeDto(Home home) {
        return HomeDto.builder()
                .home_id(home.getHome_id())
                .home_name(home.getHome_name())
                .build();
    }
}
