package com.javamaster.spring_crud.service;

import com.javamaster.spring_crud.dto.HomeDto;
import com.javamaster.spring_crud.entity.Home;
import com.javamaster.spring_crud.exception.ValidationException;

import java.util.List;

public interface HomeService {
    HomeDto saveHome(HomeDto homeDto) throws ValidationException;

    void deleteHome(Integer home_id);


    List<HomeDto> findAll();
}
