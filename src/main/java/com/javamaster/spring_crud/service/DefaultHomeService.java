package com.javamaster.spring_crud.service;


import com.javamaster.spring_crud.dto.HomeDto;
import com.javamaster.spring_crud.entity.Home;
import com.javamaster.spring_crud.exception.ValidationException;
import com.javamaster.spring_crud.repository.HomeRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DefaultHomeService implements HomeService {

    private final HomeRepository homeRepository;
    private final HomeConverter homeConverter;


    @Override
    public HomeDto saveHome(HomeDto homeDto) throws ValidationException {
        validateHomeDto(homeDto);
        Home savedHome = homeRepository.save(homeConverter.fromHomeDtoToHome(homeDto));
        return homeConverter.fromHomeToHomeDto(savedHome);
    }


    private void validateHomeDto(HomeDto homeDto) throws ValidationException {
        System.out.println("Name is empty");
    }

    @Override
    public void deleteHome(Integer home_id) {
        homeRepository.deleteById(home_id);
    }


    @Override
    public List<HomeDto> findAll() {
        return homeRepository.findAll()
                .stream()
                .map(homeConverter::fromHomeToHomeDto)
                .collect(Collectors.toList());
    }

}
