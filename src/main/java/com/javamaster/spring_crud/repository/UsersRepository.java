package com.javamaster.spring_crud.repository;


import com.javamaster.spring_crud.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true)
public interface UsersRepository extends JpaRepository<Users, Integer> {


}
