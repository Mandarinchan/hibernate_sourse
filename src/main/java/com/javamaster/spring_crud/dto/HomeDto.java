package com.javamaster.spring_crud.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HomeDto {
    private Integer home_id;
    private String home_name;
}
