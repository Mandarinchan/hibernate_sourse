package com.javamaster.spring_crud.controller;

import com.javamaster.spring_crud.dto.HomeDto;
import com.javamaster.spring_crud.exception.ValidationException;
import com.javamaster.spring_crud.service.HomeService;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/home")
@AllArgsConstructor
@Log
@CrossOrigin
public class HomeController {

    private final HomeService homeService;

    @PostMapping("/save")
    public HomeDto saveHome(@RequestBody HomeDto homeDto) throws ValidationException {
        //log.info("Handling save users: " + usersDto);
        return homeService.saveHome(homeDto);
    }

    @GetMapping("/findAll")
    public List<HomeDto> findAllHome() {
        //log.info("Handling find all users request");
        return homeService.findAll();
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteHome(@PathVariable Integer id) {
        //log.info("Handling delete user request: " + id);
        homeService.deleteHome(id);
        return ResponseEntity.ok().build();
    }

}
